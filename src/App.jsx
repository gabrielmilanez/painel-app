import "./App.css";
import Rotas from "./routes/Rotas";

function App() {
  return (
    <div id="app" className="px-4">
      <Rotas />
    </div>
  );
}

export default App;

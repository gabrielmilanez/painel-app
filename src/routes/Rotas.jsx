import { Routes, Route, BrowserRouter } from "react-router-dom";
import { Analitico, Indicadores, RealTime } from "../pages";
import { Header } from "../components";

function Rotas() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route exact path="/" element={<RealTime />} />
        <Route path="realtime" element={<RealTime />} />
        <Route path="analitico" element={<Analitico />} />
        <Route path="indicadores" element={<Indicadores />} />
      </Routes>
    </BrowserRouter>
  );
}

export default Rotas;

import React from "react";
import './RealTime.css';
import { MdOutlineWatchLater } from "react-icons/md";
import { FaArrowUp } from "react-icons/fa";
import { FiCheck } from "react-icons/fi";
import { ImCancelCircle } from "react-icons/im";

function RealTime() {
  return (
    <div className="container-realtime">
      <h1>RealTime</h1>

      <div className="content-realtime">
        <div className="cards-realtime">

          <div className="card">

            <div className="icon-img bg-orange-500">
              <MdOutlineWatchLater />
            </div>
            <div className="card-content">

              <div className="principal-info">
                <h1> Em Aberto:  </h1>
                <h1 className="ml-2"> 0 </h1>
              </div>

              <div className="secondary-info">
                <div className="info-esq">
                  <h2><b>No Prazo:</b></h2>
                  <h1> 0 </h1>
                </div>
                <div className="info-dir">
                  <h2>Em Atraso:</h2>
                  <h1> 0 </h1>
                </div>
              </div>
            </div>
          </div>

          <div className="card">

            <div className="icon-img bg-blue-600">
              <FaArrowUp />
            </div>
            <div className="card-content">

              <div className="principal-info">
                <h1> Em Atendimento:  </h1>
                <h1 className="ml-2"> 0 </h1>
              </div>

              <div className="secondary-info">
                <div className="info-esq">
                  <h2><b>No Prazo:</b></h2>
                  <h1> 0 </h1>
                </div>
                <div className="info-dir">
                  <h2>Em Atraso:</h2>
                  <h1> 0 </h1>
                </div>
              </div>
            </div>
          </div>

          <div className="card">

            <div className="icon-img bg-indigo-900">
              <FiCheck />
            </div>
            <div className="card-content">

              <div className="principal-info">
                <h1> Finalizados: </h1>
                <h1 className="ml-2"> 0 </h1>
              </div>

              <div className="secondary-info">
                <div className="info-esq">
                  <h2><b>No Prazo:</b></h2>
                  <h1> 0 </h1>
                </div>
                <div className="info-dir">
                  <h2>Em Atraso:</h2>
                  <h1> 0 </h1>
                </div>
              </div>
            </div>
          </div>

          <div className="card">

            <div className="icon-img bg-red-600">
              <ImCancelCircle />
            </div>
            <div className="card-content">

              <div className="principal-info ">
                <div className="items-center text-center">
                  <h1> Cancelados:  </h1>
                  <h1 className="ml-2"> 0 </h1>
                </div>
              </div>

            </div>
          </div>

        </div>
      </div>
    </div>
  );
}

export default RealTime;
import RealTime from "./RealTime/Realtime";
import Analitico from "./Analitico/Analitico";
import Indicadores from "./Indicadores/Indicadores";

export {
  RealTime,
  Analitico,
  Indicadores,
}
import React from "react";
import { SimpleCard, SubTitle, PanelBarChart } from "../../components/";

function Indicadores() {
  const barData = [{ solic: 2088, year: 2022 }];

  // Chart config
  const barConfig = [
    {
      key: "solic",
      color: "#111827",
    },
  ];

  return (
    <div className="grid grid-cols-1 gap-5 my-6">
      <SubTitle mainTitle="Demanda de Serviço" subTitle="Resumo Executivo" />
      <div className="flex gap-y-3 gap-x-24">
        <div className="grid grid-cols-2 grid-rows-2 gap-3 flex-1 max-w-111">
          <SimpleCard
            title="Total Solicitações"
            data={108.503}
            className="bg-gray-800"
          />
          <SimpleCard
            title="% Solicitações No Prazo"
            data={"91,36%"}
            className="bg-gray-800"
          />
          <SimpleCard
            title="1ª Necessidade No Prazo"
            data={99.125}
            className="bg-blue-400"
          />
          <SimpleCard
            title="1ª Necessidade Atrasada"
            data={9.378}
            className="bg-red-600"
          />
        </div>
        <div className="grid grid-cols-4 grid-rows-2 gap-3 flex-1">
          <SimpleCard
            title="Não Urgentes"
            data={86.185}
            className="bg-gray-800"
          />
          <SimpleCard
            title="% Não Urgentes No Prazo"
            data={"96,15%"}
            className="bg-gray-800"
          />
          <SimpleCard
            title="1ª Necessidade No Prazo Não Urgentes"
            data={82.863}
            className="bg-blue-400"
          />
          <SimpleCard
            title="1ª Necessidade Atrasada Não Urgentes"
            data={3.322}
            className="bg-red-600"
          />
          <SimpleCard title="Urgentes" data={22.318} className="bg-gray-800" />
          <SimpleCard
            title="% Urgentes No Prazo"
            data={"72,86%"}
            className="bg-gray-800"
          />
          <SimpleCard
            title="1ª Necessidade No Prazo Urgentes"
            data={16.262}
            className="bg-blue-400"
          />
          <SimpleCard
            title="1ª Necessidade Atrasada Urgentes"
            data={6.056}
            className="bg-red-600"
          />
        </div>
      </div>
      <SubTitle mainTitle="Demanda de Serviço" subTitle="Ano e Mês" />
      <div className="grid gap-24">
        <PanelBarChart
          data={barData}
          bars={barConfig}
          limit={4000}
          margin={{
            top: 10,
          }}
          x={{ dataKey: "year", axisLine: false, tickLine: false }}
          cartesiangrid={{ vertical: false }}
          className="w-80 h-42 shadow-lg rounded-lg border border-gray-100"
        />
      </div>
    </div>
  );
}

export default Indicadores;

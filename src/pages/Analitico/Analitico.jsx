import React from "react";
import './Analitico.css';
import { MdOutlineWatchLater } from "react-icons/md";
import { FaArrowUp } from "react-icons/fa";
import { FiCheck } from "react-icons/fi";
import { ImCancelCircle } from "react-icons/im";
import { FiAlertCircle } from "react-icons/fi";

function Analitico() {
  return (
    <div className="container-analitico">
      <div className="container-esq">
        <div className="cards-analitico">
          <div className="card">
            <div className="icon-img bg-indigo-900" >
              <FiCheck />
            </div>
            <div className="card-info">
              <h1 className="text-xs"> Finalizadas:  </h1>
              <h1 className="ml-2"> 0 </h1>
            </div>
          </div>
          <div className="card">
            <div className="icon-img bg-orange-500" >
              <MdOutlineWatchLater />
            </div>
            <div className="card-info">
              <h1 className="text-xs"> Abertas:  </h1>
              <h1 className="ml-2"> 0 </h1>
            </div>
          </div>
          <div className="card">
            <div className="icon-img bg-blue-600" >
            <FaArrowUp />
            </div>
            <div className="card-info">
              <h1 className="text-xs"> Atendimento:  </h1>
              <h1 className="ml-2"> 0 </h1>
            </div>
          </div>
          <div className="card">
            <div className="icon-img bg-red-600" >
              <FiAlertCircle />
            </div>
            <div className="card-info">
              <h1 className="text-xs"> Atrasadas:  </h1>
              <h1 className="ml-2"> 0 </h1>
            </div>
          </div>
          <div className="card">
            <div className="icon-img bg-red-600" >
            <ImCancelCircle />
            </div>
            <div className="card-info">
              <h1 className="text-xs"> Canceladas:  </h1>
              <h1 className="ml-2"> 0 </h1>
            </div>
          </div>
        </div>
      </div>
      <div className="container-dir">
        <h1>Analítico</h1>
      </div>

    </div >
  );
}

export default Analitico;
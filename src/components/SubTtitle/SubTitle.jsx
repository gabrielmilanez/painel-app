import React from "react";

const SubTitle = ({ mainTitle, subTitle }) => {
  return (
    <h2 className="text-sm font-open-sans text-gray-900">
      {mainTitle}
      {!!subTitle && <span className="text-gray-400">| {subTitle}</span>}
    </h2>
  );
};

export default SubTitle;

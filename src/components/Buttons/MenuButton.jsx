import React from "react";
import { NavLink } from "react-router-dom";

const MenuButton = ({ to = "#", children, className }) => {
  return (
    <NavLink
      to={to}
      className={({ isActive }) => {
        return `${className} py-1 px-2 text-sm font-open-sans border rounded-lg ${
          isActive
            ? "text-white bg-blue-500 border-blue-500"
            : "text-gray-600 border-gray-300 bg-white"
        }`;
      }}
    >
      {children}
    </NavLink>
  );
};

export default MenuButton;

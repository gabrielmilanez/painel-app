import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  ResponsiveContainer,
  Legend,
  LabelList,
  CartesianGrid,
} from "recharts";

const PanelBarChart = ({
  data,
  bars,
  legend,
  className,
  layout = "horizontal",
  radius = 0,
  barSize,
  margin = {},
  animation = true,
  cartesiangrid,
  x,
  y,
  label,
  limit,
  shape,
}) => {
  const barsComponent = bars?.map((bar, i) => {
    return (
      <Bar
        key={bar.key}
        shape={shape || false}
        dataKey={bar.key}
        fill={bar.color || "#fff"}
        radius={radius}
        stackId={bar.stackId || "stack-" + i}
        maxBarSize={barSize}
        isAnimationActive={animation}
      >
        {label && <LabelList dataKey={bar.key} {...label} />}
      </Bar>
    );
  });

  return (
    <div className={className}>
      <ResponsiveContainer width="100%" height="100%">
        <BarChart data={data} layout={layout} margin={margin}>
          <CartesianGrid {...cartesiangrid} />
          {barsComponent}
          <XAxis {...x} domain={[0, limit || x?.dataKey]} />
          <YAxis {...y} domain={[0, limit || y?.dataKey]} />
          {!!legend && (
            <Legend
              layout={legend.layout || "horizontal"}
              payload={legend.titles?.map((title) => ({
                type: title.icon || "square",
                value: (
                  <span className="font-open-sans text-xsm text-white ml-1 text-center">
                    {title.title}
                  </span>
                ),
                color: title.color || "#FFF",
              }))}
              verticalAlign={legend.verticalAlign || "top"}
              iconSize={legend.iconSize || "8px"}
              align={legend.align || "right"}
              wrapperStyle={legend.position || {}}
            />
          )}
        </BarChart>
      </ResponsiveContainer>
    </div>
  );
};

export default PanelBarChart;

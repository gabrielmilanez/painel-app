import React from "react";

const SimpleCard = ({ title, data, className }) => {
  return (
    <div
      className={`${className} flex flex-col items-center p-2 rounded-xl shadow-lg font-roboto`}
    >
      <p className="text-white text-sm text-center">{title}</p>
      <span className="text-white font-normal text-3xl">{data}</span>
    </div>
  );
};

export default SimpleCard;

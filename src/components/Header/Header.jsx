import React from "react";
import { MenuButton } from "../";
import ThreeWings from "../../assets/img/3Wings.png";

const Header = () => {
  return (
    <header>
      <div className="w-full flex justify-between">
        <div className="flex items-center gap-5">
          <h1 className="font-open-sans font-light text-7xl tracking-wide text-blue-700">
            fibbo
          </h1>
          <nav className="flex gap-1 mt-4">
            <MenuButton to="/realtime">Real Time</MenuButton>
            <MenuButton to="/analitico">Analítico</MenuButton>
            <MenuButton to="/indicadores">Indicadores</MenuButton>
          </nav>
        </div>
        <div className="flex items-center gap-5">
          <p className="text-xl font-open-sans font-normal text-blue-900">
            Moinhos de Vento | Farmácia
          </p>
          <div>
            <p className="mb-2 text-xs text-gray-400">powered by</p>
            <img src={ThreeWings} alt="3wings logo" className="w-14 h-4" />
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
